package com.github.yuxiaobin.sample.boot.mp2.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.github.yuxiaobin.sample.boot.mp2.entity.H2user;
import com.github.yuxiaobin.sample.boot.mp2.mapper.H2userMapper;

/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2019/2/19
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AppTest {

    @Autowired
    H2userMapper userMapper;

    @Test
    public void test() {
        userMapper.selectPage(new Page<H2user>(1,3),new EntityWrapper<H2user>()
                .gt("age", 1))
                .forEach(System.out::println);
    }

}
