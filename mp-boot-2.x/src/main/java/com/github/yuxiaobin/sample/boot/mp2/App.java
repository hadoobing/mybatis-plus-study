package com.github.yuxiaobin.sample.boot.mp2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2019/2/19
 */
@SpringBootApplication
//@MapperScan("com.github.yuxiaobin.sample.boot.mp2.mapper")
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

}
