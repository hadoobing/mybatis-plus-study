package com.github.yuxiaobin.sample.boot.mp2.config;

import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.baomidou.mybatisplus.plugins.PaginationInterceptor;

/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2019/2/19
 */
@Configuration
public class MpConfig {

    /**
     * 分页插件，通过@Bean方式注入
     *
     * @return
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }

    @Bean
    public MapperScannerConfigurer mapperScannerConfigurer() {
        MapperScannerConfigurer scannerConfigurer = new MapperScannerConfigurer();
        scannerConfigurer.setBasePackage("com.github.yuxiaobin.sample.boot.*.mapper");
        return scannerConfigurer;
    }

}
