package com.github.yuxiaobin.mpsample3.core.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.yuxiaobin.mpsample3.core.entity.H2user;
import com.github.yuxiaobin.mpsample3.core.mapper.H2userMapper;
import com.github.yuxiaobin.mpsample3.core.service.IH2userService;
import com.github.yuxiaobin.sample.test.base.BaseTest;


/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2019/1/31
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:application-context-crud.xml"})
public class MpCRUDTest extends BaseTest {

    @Autowired
    IH2userService userService;

    @Autowired
    H2userMapper userMapper;

    @Test
    public void Create() {
        H2user user = new H2user()
                .setName("Abc")
                .setAge(18);
        Assert.assertTrue("create Abc successfully", userService.save(user));
        Assert.assertNotNull("id is auto increment", user.getId());
        Assert.assertEquals("age should be 18", 18, userService.getById(user.getId()).getAge().intValue());
    }

    @Test
    public void Retrieve() {
        userService.save(new H2user().setName("Def").setAge(20));
        /*
         select * from h2user where name like '%ef' and age>=18
         */
        Wrapper<H2user> ew = new QueryWrapper<H2user>().lambda()
                .likeLeft(H2user::getName, "ef")
                .ge(H2user::getAge, 18);
        int count = userMapper.selectCount(ew);
        Assert.assertTrue("count should not be 0", count != 0);
        H2user user = userMapper.selectList(ew).get(0);
        Assert.assertTrue("age should be greater than 18", user.getAge() > 18);
    }

    @Test
    public void Update() {
        H2user user = new H2user().setName("Tom").setAge(22);
        userService.save(user);
        Long id = user.getId();

        user.setName("Jerry");
        /*
          update h2user set name='Jerry',age=22 where id=?
         */
        userService.updateById(user);
        H2user userUpdated = userService.getById(id);
        Assert.assertEquals("name should be updated", "Jerry", userUpdated.getName());

        /*
         update h2user set age=18 where name='Jerry'
         */
        Wrapper<H2user> ew = new QueryWrapper<H2user>().lambda()
                .eq(H2user::getName, "Jerry");
        H2user updateEt = new H2user().setAge(18);
        userService.update(updateEt, ew);

        userUpdated = userService.getById(id);
        Assert.assertEquals("name should be updated", "Jerry", userUpdated.getName());
    }

    @Test
    public void Delete() {
        H2user user = new H2user().setName("Tony").setAge(20);
        userService.save(user);
        Long id = user.getId();

        Wrapper<H2user> ew = new QueryWrapper<H2user>().lambda()
                .eq(H2user::getName, "Tony");
        int count = userService.count(ew);
        Assert.assertEquals("Tony should be created", 1, count);

        /*
        delete from h2user where id=?
         */
        userService.removeById(id);

        count = userService.count(ew);
        Assert.assertEquals("Tony should be deleted", 0, count);

        userService.save(user);
        count = userService.count(ew);
        Assert.assertEquals("Tony should be created again", 1, count);
        /*
        delete from h2user where name like '%Tony%';
         */
        boolean success = userService.remove(ew);
        Assert.assertTrue("Tony should be deleted again successfully", success);
        count = userService.count(ew);
        Assert.assertEquals("Tony should be deleted again", 0, count);
    }


    @Test
    public void chainMethods() {
        /*
         查询已字母a开头的年龄>3的记录
         */
        userService.lambdaQuery()
                .likeRight(H2user::getName, "a")
                .gt(H2user::getAge, 3)
                .list().forEach(System.out::println);
    }


}
