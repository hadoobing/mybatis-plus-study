package com.github.yuxiaobin.mpsample3.core.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.github.yuxiaobin.mpsample3.core.entity.H2user;

/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2019/1/31
 */
public interface H2userMapper extends BaseMapper<H2user> {

}
