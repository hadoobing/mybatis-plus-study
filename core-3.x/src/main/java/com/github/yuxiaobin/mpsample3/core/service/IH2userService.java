package com.github.yuxiaobin.mpsample3.core.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.github.yuxiaobin.mpsample3.core.entity.H2user;

/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2019/1/31
 */
public interface IH2userService extends IService<H2user> {

}
