package com.github.yuxiaobin.sample.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.github.yuxiaobin.sample.jdbc.entity.H2user;
import com.github.yuxiaobin.sample.test.base.BaseTest;

/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2019/2/15
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:application-context-jdbc.xml"})
public class JdbcTemplateTest extends BaseTest {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedJdbcTemplate;

    @Test
    public void Create() {
        String sql = "insert into h2user(name, age) values (?,?)";
        jdbcTemplate.update(sql, "Tomy", 10);

        String sql2 = "insert into h2user(name, age) values(:name,:age)";
        Map<String, Object> param = new HashMap<>(2);
        param.put("name", "Tomy");
        param.put("age", 11);
        namedJdbcTemplate.update(sql2, param);

        String sql3 = "select * from h2user where name like 'Tomy%'";
        jdbcTemplate.query(sql3, new RowMapper<H2user>() {
                    @Override
                    public H2user mapRow(ResultSet rs, int rowNum) throws SQLException {
                        return new H2user()
                                .setId(rs.getLong("ID"))
                                .setName(rs.getString("NAME"))
                                .setAge((Integer) rs.getObject("AGE"));
                    }
                }
        ).forEach(System.out::println);
    }

    @Test
    public void Retrieve() {
        String sql = "select * from h2user where name like concat(concat('%',?),'%')";
        List<H2user> userList = jdbcTemplate.query(sql, new String[]{"a"},
                (resultSet, i) -> new H2user()
                        .setId(resultSet.getLong("ID"))
                        .setName(resultSet.getString("NAME"))
                        .setAge((Integer) resultSet.getObject("AGE")));
        System.out.println("userList.size=" + userList.size());
        for (H2user user : userList) {
            System.out.println(user);
        }

        String sql2 = "select * from h2user where name like :nameLike";
        Map<String, Object> param = new HashMap<>(1);
        param.put("nameLike", "%a%");
        List<H2user> userList2 = namedJdbcTemplate.query(sql2, param,
                (rs, rowNum) -> new H2user()
                        .setId(rs.getLong("ID"))
                        .setName(rs.getString("NAME"))
                        .setAge((Integer) rs.getObject("AGE")));
        System.out.println("userList2.size=" + userList2.size());
        for (H2user user : userList2) {
            System.out.println(user);
        }
    }


}
