package com.github.yuxiaobin.sample.mybatis.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.session.RowBounds;

import com.github.yuxiaobin.sample.mybatis.entity.H2user;

/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2019/2/14
 */
public interface H2userMapper {

    /**
     * Mybatis原生分页
     * @param rowBounds
     * @param param
     * @return
     */
    @Select("select * from h2user")
    public List<H2user> selectPage(RowBounds rowBounds, Map<String, Object> param);

    /**
     * 查询所有记录
     * @return
     */
    @Select("select * from h2user")
    public List<H2user> selectList();

    /**
     * 写sql在xml里面
     *
     * @param nameLike
     * @return
     */
    public List<H2user> selectByCondition(@Param("nameLike") String nameLike);

    @Select("select * from h2user where id=#{id}")
    public H2user selectById(@Param("id") Long id);

    @Insert({"insert into h2user(name,age) values(#{name}, #{age})"})
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    public int insert(H2user user);

    @Update("update h2user set name=#{name}, age=#{age} where id=#{id}")
    public int updateById(H2user user);

    List<H2user> getListByOrder(@Param("ageRange") int age);

}
