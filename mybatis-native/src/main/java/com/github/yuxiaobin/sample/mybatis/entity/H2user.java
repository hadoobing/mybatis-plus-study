package com.github.yuxiaobin.sample.mybatis.entity;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2019/2/14
 */
@Data
@Accessors(chain = true)
public class H2user {

    private Long id;

    private String name;

    private Integer age;

}
