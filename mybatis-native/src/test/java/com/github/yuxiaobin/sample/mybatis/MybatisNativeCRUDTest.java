package com.github.yuxiaobin.sample.mybatis;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.github.yuxiaobin.sample.mybatis.entity.H2user;
import com.github.yuxiaobin.sample.mybatis.mapper.H2userMapper;
import com.github.yuxiaobin.sample.test.base.BaseTest;

/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2019/2/14
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:application-context-native.xml"})
public class MybatisNativeCRUDTest extends BaseTest {


    @Autowired
    H2userMapper userMapper;

    @Test
    public void nativeCRUD() {
        H2user user = new H2user().setName("nativeCRUD").setAge(11);
        Assert.assertEquals(1, userMapper.insert(user));
        Long id = user.getId();
        System.out.println("insert user=" + user);
        user = userMapper.selectById(id);
        Assert.assertEquals(11, user.getAge().intValue());

        user.setAge(20);
        Assert.assertEquals(1, userMapper.updateById(user));
        System.out.println("update user=" + userMapper.selectById(id));

        /**
         * like
         */
        List<H2user> userLikeList = userMapper.selectByCondition("tive");
        System.out.println("userLikeList.size=" + userLikeList.size());
        Assert.assertEquals(1, userLikeList.size());

        Assert.assertTrue(userMapper.selectByCondition("abc").isEmpty());

        userLikeList = userMapper.selectByCondition("");
        System.out.println("userLikeList for \"\"=" + userLikeList.size());
        Assert.assertTrue(!userLikeList.isEmpty());

    }

    @Test
    public void testOrderByVariable(){
        userMapper.getListByOrder(3).forEach(System.out::println);
    }


}
