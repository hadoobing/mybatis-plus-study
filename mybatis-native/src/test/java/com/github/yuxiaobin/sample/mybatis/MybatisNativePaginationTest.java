package com.github.yuxiaobin.sample.mybatis;


import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.github.yuxiaobin.sample.mybatis.entity.H2user;
import com.github.yuxiaobin.sample.mybatis.mapper.H2userMapper;
import com.github.yuxiaobin.sample.test.base.BaseTest;

/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2019/2/14
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:application-context-native.xml"})
public class MybatisNativePaginationTest extends BaseTest{

    @Autowired
    H2userMapper userMapper;

    @Test
    public void nativePagination() {
        Map<String, Object> param = Collections.EMPTY_MAP;
        List<H2user> list = userMapper.selectPage(new RowBounds(1, 3), param);
        System.out.println("pageList.size=" + list.size());
        Assert.assertEquals(3, list.size());

        List<H2user> listAll = userMapper.selectList();
        System.out.println("listAll.size=" + listAll.size());
        Assert.assertTrue(listAll.size() > 3);
    }



}
