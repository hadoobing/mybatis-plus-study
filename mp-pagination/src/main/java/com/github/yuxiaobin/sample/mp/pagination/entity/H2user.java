package com.github.yuxiaobin.sample.mp.pagination.entity;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2019/1/31
 */
@Data
@Accessors(chain = true)
public class H2user {

    private Long id;

    private String name;

    private Integer age;

}
