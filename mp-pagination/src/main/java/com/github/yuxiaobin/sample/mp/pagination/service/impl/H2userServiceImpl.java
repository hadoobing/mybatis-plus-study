package com.github.yuxiaobin.sample.mp.pagination.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.yuxiaobin.sample.mp.pagination.entity.H2user;
import com.github.yuxiaobin.sample.mp.pagination.mapper.H2userMapper;
import com.github.yuxiaobin.sample.mp.pagination.service.IH2userService;

/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2019/1/31
 */
@Service
public class H2userServiceImpl extends ServiceImpl<H2userMapper, H2user> implements IH2userService {

}
