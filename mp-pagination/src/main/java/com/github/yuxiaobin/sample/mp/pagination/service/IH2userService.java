package com.github.yuxiaobin.sample.mp.pagination.service;

import com.baomidou.mybatisplus.service.IService;
import com.github.yuxiaobin.sample.mp.pagination.entity.H2user;

/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2019/1/31
 */
public interface IH2userService extends IService<H2user> {

}
