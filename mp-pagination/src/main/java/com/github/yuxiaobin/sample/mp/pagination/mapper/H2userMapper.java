package com.github.yuxiaobin.sample.mp.pagination.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.github.yuxiaobin.sample.mp.pagination.entity.H2user;

/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2019/1/31
 */
public interface H2userMapper extends BaseMapper<H2user> {

    public List<H2user> selectMyPage(Page<H2user> page, @Param("nameLike") String nameLike);

    public Long selectMyCount(@Param("nameLike") String nameLike);

}
