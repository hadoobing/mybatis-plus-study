package com.github.yuxiaobin.sample.mp.pagination.test;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.github.yuxiaobin.sample.mp.pagination.entity.H2user;
import com.github.yuxiaobin.sample.mp.pagination.mapper.H2userMapper;
import com.github.yuxiaobin.sample.mp.pagination.service.IH2userService;
import com.github.yuxiaobin.sample.test.base.BaseTest;

/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2019/1/31
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:application-context-pagination.xml"})
public class MpPaginationTest extends BaseTest {

    @Autowired
    H2userMapper userMapper;
    @Autowired
    IH2userService userService;

    @Test
    public void paginationTest() {
        String nameLike = "a";
        EntityWrapper<H2user> ew = new EntityWrapper<>();
        ew.like("name", nameLike);
        Page<H2user> page = new Page<>(1, 3);
        userMapper.selectPage(page, ew);
        List<H2user> list1 = page.getRecords();
        long count1 = page.getTotal();
        long pages1 = page.getPages();
        System.out.println("count1=" + count1 + ", pages1=" + pages1 + ", list1.size=" + list1.size());

        /**
         * 自定义SQL的分页
         */
        Page<H2user> page2 = new Page<>(1, 3);
        userMapper.selectMyPage(page2, nameLike);
        Assert.assertEquals(count1, page2.getTotal());
        Assert.assertEquals(pages1, page2.getPages());
        Assert.assertEquals(list1.size(), page2.getRecords().size());
    }


    @Test
    public void paginationWithoutCount() {
        String nameLike = "a";

        EntityWrapper<H2user> ew = new EntityWrapper<>();
        ew.like("name", nameLike);
        Page<H2user> page = new Page<>(1, 3);
        userService.selectPage(page, ew);
        List<H2user> list1 = page.getRecords();
        long count1 = page.getTotal();
        long pages1 = page.getPages();
        System.out.println("count1=" + count1 + ", pages1=" + pages1 + ", list1.size=" + list1.size());

        Page<H2user> page2 = new Page<>(1, 3);
        /*
          不需要mp来查总数
         */
        page2.setSearchCount(false);
        page2.setRecords(userMapper.selectMyPage(page2, nameLike));
        Assert.assertEquals("没有查总数，所以total=0", 0, page2.getTotal());
        Assert.assertEquals(list1.size(), page2.getRecords().size());
        /*
        自定义sql查总数
         */
        long count2 = userMapper.selectMyCount(nameLike);
        Assert.assertEquals(count1, count2);
        page2.setTotal(count2);
        /*
         把总数设置到page, 就可以计算总页数
         */
        Assert.assertEquals(pages1, page2.getPages());
    }

}
