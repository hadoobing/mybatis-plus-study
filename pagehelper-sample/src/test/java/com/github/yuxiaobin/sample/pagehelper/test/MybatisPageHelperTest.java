package com.github.yuxiaobin.sample.pagehelper.test;

import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageRowBounds;
import com.github.yuxiaobin.sample.pagehelper.entity.H2user;
import com.github.yuxiaobin.sample.pagehelper.mapper.H2userMapper;
import com.github.yuxiaobin.sample.test.base.BaseTest;

/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2019/2/14
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:application-context-pagehelper.xml"})
public class MybatisPageHelperTest extends BaseTest {

    @Autowired
    H2userMapper userMapper;

    @Test
    public void rowBoundsPagination() {
        PageRowBounds pageRowBounds = new PageRowBounds(1, 3);
        List<H2user> list = userMapper.selectPage(pageRowBounds, Collections.EMPTY_MAP);
        Assert.assertNotNull(pageRowBounds.getTotal());
        Assert.assertNotEquals(0, pageRowBounds.getTotal().longValue());
        Assert.assertEquals(3, list.size());
    }

    @Test
    public void noRowBoundsPagination() {
        PageHelper.startPage(1, 3);
        List<H2user> list = userMapper.selectList();
        Assert.assertEquals(3, list.size());

        System.out.println("Search Count = false");
        PageHelper.startPage(1, 3, false);
        PageHelper.orderBy("id");
        list = userMapper.selectList();
        Assert.assertEquals(3, list.size());

        list = userMapper.selectList();
        Assert.assertNotEquals(3, list.size());
    }


}
