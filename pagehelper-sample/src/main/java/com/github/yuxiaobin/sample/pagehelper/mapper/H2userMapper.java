package com.github.yuxiaobin.sample.pagehelper.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;

import com.github.yuxiaobin.sample.pagehelper.entity.H2user;


/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2019/2/14
 */
public interface H2userMapper {

    /**
     * 演示RowBounds分页
     * @param rowBounds
     * @param param
     * @return
     */
    @Select("select * from h2user")
    public List<H2user> selectPage(RowBounds rowBounds, Map<String, Object> param);

    /**
     * 演示无RouBounds参数的分页查询
     * @return
     */
    @Select("select * from h2user")
    public List<H2user> selectList();

    /**
     * 写sql在xml里面
     *
     * @param nameLike
     * @return
     */
    public List<H2user> selectByCondition(@Param("nameLike") String nameLike);


}
