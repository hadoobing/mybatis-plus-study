package com.github.yuxiaobin.mpsample2.core.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baomidou.mybatisplus.enums.SqlLike;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.github.yuxiaobin.mpsample2.core.entity.H2user;
import com.github.yuxiaobin.mpsample2.core.mapper.H2userMapper;
import com.github.yuxiaobin.mpsample2.core.service.IH2userService;

/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2019/1/31
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:application-context-crud.xml"})
public class MpCRUDTest {

    @Autowired
    IH2userService userService;

    @Autowired
    H2userMapper userMapper;

    @Test
    public void Create() {
        H2user user = new H2user()
                .setName("Abc")
                .setAge(18);
        Assert.assertTrue("create Abc successfully", userService.insert(user));
        Assert.assertNotNull("id is auto increment", user.getId());
        Assert.assertEquals("age should be 18", 18, userService.selectById(user.getId()).getAge().intValue());
    }

    @Test
    public void Retrieve() {
        userService.insert(new H2user().setName("Def").setAge(20));
        EntityWrapper<H2user> ew = new EntityWrapper<>();
        /*
         select * from h2user where name like '%ef' and age>=18
         */
        ew.like("firstName", "ef", SqlLike.LEFT).ge("age", 18);
        int count = userMapper.selectCount(ew);
        Assert.assertTrue("count should not be 0", count != 0);
        H2user user = userMapper.selectList(ew).get(0);
        Assert.assertTrue("age should be greater than 18", user.getAge() > 18);
    }

    @Test
    public void Update() {
        H2user user = new H2user().setName("Tom").setAge(22);
        userService.insert(user);
        Long id = user.getId();

        user.setName("Jerry");
        /*
          update h2user set name='Jerry',age=22 where id=?
         */
        userService.updateById(user);
        H2user userUpdated = userService.selectById(id);
        Assert.assertEquals("name should be updated", "Jerry", userUpdated.getName());

        /*
         update h2user set age=18 where name='Jerry'
         */
        EntityWrapper<H2user> updateEw = new EntityWrapper<>();
        updateEw.eq("firstName", "Jerry");
        H2user updateEt = new H2user().setAge(18);
        userService.update(updateEt, updateEw);

        userUpdated = userService.selectById(id);
        Assert.assertEquals("name should be updated", "Jerry", userUpdated.getName());
    }

    @Test
    public void Delete() {
        H2user user = new H2user().setName("Tony").setAge(20);
        userService.insert(user);
        Long id = user.getId();

        EntityWrapper<H2user> ew = new EntityWrapper<>();
        ew.like("firstName", "Tony");
        int count = userService.selectCount(ew);
        Assert.assertEquals("Tony should be created", 1, count);

        /*
        delete from h2user where id=?
         */
        userService.deleteById(id);

        count = userService.selectCount(ew);
        Assert.assertEquals("Tony should be deleted", 0, count);

        userService.insert(user);
        count = userService.selectCount(ew);
        Assert.assertEquals("Tony should be created again", 1, count);
        /*
        delete from h2user where name like '%Tony%';
         */
        boolean success = userService.delete(ew);
        Assert.assertTrue("Tony should be deleted again successfully", success);
        count = userService.selectCount(ew);
        Assert.assertEquals("Tony should be deleted again", 0, count);

        userService.selectList(new EntityWrapper<H2user>()
            .setSqlSelect("id, firstName")
                .like("firstName", "a", SqlLike.RIGHT)
                .gt("age", 18)
        );
    }


}
