package com.github.yuxiaobin.mpsample2.core.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.github.yuxiaobin.mpsample2.core.entity.H2user;
import com.github.yuxiaobin.mpsample2.core.service.IH2userService;

/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2019/7/30
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:application-context-crud.xml"})
public class MyIServiceTest {

    @Autowired
    IH2userService userService;

    @Test
    public void test(){
        List<H2user> entityList = new ArrayList<>();
        entityList.add(new H2user().setAge(3));
        List<H2user> list = userService.listBatchByEntityList(entityList);
        System.out.println(list.size());
    }

    @Test
    public void test2(){
        userService.selectList(new EntityWrapper<H2user>(new H2user().setAge(3)))
                .forEach(System.out::println);
    }

}
