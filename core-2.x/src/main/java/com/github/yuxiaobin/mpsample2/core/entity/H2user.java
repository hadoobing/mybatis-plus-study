package com.github.yuxiaobin.mpsample2.core.entity;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2019/1/31
 */
@Data
@Accessors(chain = true)
public class H2user {

    private Long id;

    private String firstName;

    private Integer age;

    public H2user setName(String name){
        this.firstName = name;
        return this;
    }

    public String getName(){
        return this.firstName;
    }
}
