package com.github.yuxiaobin.mpsample2.core.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.binding.MapperMethod;
import org.apache.ibatis.session.SqlSession;

import com.baomidou.mybatisplus.enums.SqlMethod;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2019/7/30
 */
public abstract class MyServiceImpl<M extends BaseMapper<T>, T> extends ServiceImpl<M, T> {

    public List<T> listBatchByEntityList(List<T> entityList) {
        try (final SqlSession batchSqlSession = sqlSessionBatch()) {
            final int size = entityList.size();
            final int batchSize = 30;
            final List<T> result = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                final String sqlStatement = sqlStatement(SqlMethod.SELECT_LIST);
                final Map<String, Object> param = new MapperMethod.ParamMap<Object>();
                EntityWrapper<T> ew = new EntityWrapper<>(entityList.get(i));
                param.put("ew", ew);
                param.put("param1", ew);
                final List<T> list = batchSqlSession.selectList(sqlStatement, param);
                result.addAll(list);
                if (i >= 1 && i % batchSize == 0) {
                    batchSqlSession.flushStatements();
                }
            }
            batchSqlSession.flushStatements();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
