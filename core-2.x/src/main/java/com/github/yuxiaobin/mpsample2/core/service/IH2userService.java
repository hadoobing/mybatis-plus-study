package com.github.yuxiaobin.mpsample2.core.service;

import com.github.yuxiaobin.mpsample2.core.entity.H2user;

/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2019/1/31
 */
public interface IH2userService extends MyIService<H2user> {

}
