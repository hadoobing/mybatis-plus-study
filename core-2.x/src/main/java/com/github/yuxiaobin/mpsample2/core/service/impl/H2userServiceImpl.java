package com.github.yuxiaobin.mpsample2.core.service.impl;

import org.springframework.stereotype.Service;

import com.github.yuxiaobin.mpsample2.core.entity.H2user;
import com.github.yuxiaobin.mpsample2.core.mapper.H2userMapper;
import com.github.yuxiaobin.mpsample2.core.service.IH2userService;

/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2019/1/31
 */
@Service
public class H2userServiceImpl extends MyServiceImpl<H2userMapper, H2user> implements IH2userService {



}
