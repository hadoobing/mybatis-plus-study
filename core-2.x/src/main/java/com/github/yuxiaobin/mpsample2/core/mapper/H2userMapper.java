package com.github.yuxiaobin.mpsample2.core.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.github.yuxiaobin.mpsample2.core.entity.H2user;

/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2019/1/31
 */
public interface H2userMapper extends BaseMapper<H2user>{

}
