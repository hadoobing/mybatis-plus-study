package com.github.yuxiaobin.mpsample2.core.service;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * TODO class
 * </p>
 *
 * @author yuxiaobin
 * @date 2019/7/30
 */
public interface MyIService <T> extends IService<T> {

    public List<T> listBatchByEntityList(List<T> entityList);
}
