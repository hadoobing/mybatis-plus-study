package com.github.yuxiaobin.mp3boottest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.github.yuxiaobin.mp3boot.Mp3BootApp;
import com.github.yuxiaobin.mp3boot.entity.H2user;
import com.github.yuxiaobin.mp3boot.mapper.H2userMapper;

/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2020/12/10
 */
@SpringBootTest(classes = Mp3BootApp.class)
@RunWith(SpringRunner.class)
public class AppTest {

    @Autowired
    H2userMapper userMapper;

    @Test
    public void test1() {
        H2user user = new H2user().setAge(18);
        userMapper.insert(user);
        user  = userMapper.selectById(user.getId());
        System.out.println(user);
        Assert.assertNotNull(user.getCreateTime());
        Assert.assertNull(user.getUpdateTime());
        userMapper.updateById(user.setName("123"));
        user  = userMapper.selectById(user.getId());
        System.out.println(user);
        Assert.assertNotNull(user.getUpdateTime());
    }
}
