package com.github.yuxiaobin.mp3boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2019/2/19
 */
@SpringBootApplication
public class Mp3BootApp {

    public static void main(String[] args) {
        SpringApplication.run(Mp3BootApp.class, args);
    }

}
