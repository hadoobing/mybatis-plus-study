package com.github.yuxiaobin.mp3boot.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2020/12/10
 */
@Configuration
@MapperScan("com.github.yuxiaobin.mp3boot.mapper")
public class Mp3bootConfig {

}
