package com.github.yuxiaobin.mp3boot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.github.yuxiaobin.mp3boot.entity.H2user;

/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2020/12/10
 */
public interface H2userMapper extends BaseMapper<H2user> {
}
