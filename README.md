# Mybatis-plus学习示例

* 包括2.x和3.x基本CRUD示例
* 插件及其他特性，请参考官方提供的[samples](https://gitee.com/baomidou/mybatis-plus-samples)
* mybatis-native是使用mybatis原生的示例
* jdbc-template是Spring的jdbcTemplate的示例
* spring-jpa-sample是spring-data-jpa的示例
* pagehelper-sample是分页插件pagehelper的示例
* mp-pagination是mybatis-plus自带分页插件的演示


新建子模块
mvn archetype:generate -DgroupId=com.github.yuxiaobin -DarchetypeCatalog=internal -DartifactId=new-module


1. this is test
2. test2
3. test3

3.5  aaaaa
3.8  88888

4. hahaha
5. lalalal
6. ddddd


7. testaaa