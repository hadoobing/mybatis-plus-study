package com.github.yuxiaobin.sample.springdatajpa.test;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.github.yuxiaobin.sample.springdatajpa.domain.H2user;
import com.github.yuxiaobin.sample.springdatajpa.repo.H2userRepository;

/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2019/2/15
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:application-context-jpa.xml"})
public class SpringdatajpaTest {

    @Autowired
    H2userRepository userRepository;

    @Test
    public void Create() {
        userRepository.save(new H2user().setName("jpa").setAge(20));
    }

    @Test
    public void Retrieve() {
        H2user user = new H2user().setName("data").setAge(20);
        userRepository.save(user);
        Long id = user.getId();

        List<H2user> userList = userRepository.findByName("data");

        Assert.assertTrue(userList != null && !userList.isEmpty());
        Assert.assertEquals(id.longValue(), userList.get(0).getId().longValue());

        System.out.println("findAllByNameAndAge");
        userRepository.findAllByNameAndAge("data", 20).forEach(System.out::println);

        System.out.println("findUserList4NativeSql");
        userRepository.findUserList4NativeSql("data", 20).forEach(System.out::println);

        System.out.println("findUserList4Hql");
        userRepository.findUserList4Hql("data", 20).forEach(System.out::println);
    }

    @Test
    public void Update() {
        H2user user = new H2user().setName("data").setAge(20);
        userRepository.save(user);
        Long id = user.getId();

        String updatename = "data2";
        H2user updateUser = new H2user().setId(id).setName(updatename).setAge(20);
        H2user user2 = userRepository.save(updateUser);
        Assert.assertEquals(updatename, user2.getName());

    }


}
