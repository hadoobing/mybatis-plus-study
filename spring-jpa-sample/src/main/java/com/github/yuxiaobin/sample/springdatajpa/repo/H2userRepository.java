package com.github.yuxiaobin.sample.springdatajpa.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.github.yuxiaobin.sample.springdatajpa.domain.H2user;

/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2019/2/15
 */
public interface H2userRepository extends JpaRepository<H2user, Long> {

    List<H2user> findByName(String name);

    List<H2user> findAllByNameAndAge(String name, Integer age);

    @Query(value = "select * from h2user where name=?1 and age=?2", nativeQuery = true)
    List<H2user> findUserList4NativeSql(String name, Integer age);

    @Query("select u from H2user u where u.name=?1 and age=?2")
    List<H2user> findUserList4Hql(String name, Integer age);
}
