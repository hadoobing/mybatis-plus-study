package com.github.yuxiaobin.sample.test.base;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2019/2/15
 */
public abstract class BaseTest {

    @Autowired
    DataSource ds;

    @Before
    public void init() throws IOException, SQLException {
        try (Connection conn = ds.getConnection()) {
            initData(conn, "user.data.sql", "h2user");
        }
    }

    public void initData(Connection conn, String sqlFileName, String tableName) throws SQLException, IOException {
        Statement stmt = conn.createStatement();
        stmt.execute("truncate table " + tableName);
        executeSql(stmt, sqlFileName);
        conn.commit();
    }

    public void executeSql(Statement stmt, String sqlFilename) throws SQLException, IOException {
        String resourcePath = "/h2/" + sqlFilename;
        URL url = this.getClass().getResource(resourcePath);
        if (url == null) {
            throw new RuntimeException("no resouce found for " + resourcePath);
        }
        String filePath = url.getPath();
        try (
                BufferedReader reader = new BufferedReader(new FileReader(filePath))
        ) {
            String line;
            while ((line = reader.readLine()) != null) {
                stmt.execute(line.replace(";", ""));
            }
        }
    }


}
